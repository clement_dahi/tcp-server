#include	"Server.hpp"

Server::Server(int port)
{
	listeningPort = port;
	serverStatus = false;
}

Server::~Server()
{
	close(listeningSocket);
	close(connectionSocket);
}

bool	Server::isStarted()const
{
	return serverStatus;
}

void	Server::setListeningPort(int port)
{
	listeningPort = port;
}

int	Server::getListeningPort()const
{
	return listeningPort;
}

int	Server::start()
{
	if ((listeningSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror("socket() error");
		return -1;
	}

	memset(&serverAddr, 0, sizeof(serverAddr));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(listeningPort);

	if (bind(listeningSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) == -1)
	{
		perror("bind() error");
		close(listeningSocket);
		return -1;
	}

	if (listen(listeningSocket, 10) == -1)
	{
		perror("listen() error");
		close(listeningSocket);
		return -1;
	}

	serverStatus = true;

	cout << "Listening on port " << listeningPort << "..." << endl;

	while (serverStatus)
	{
		socketLength = sizeof(listeningSocket);
		connectionSocket = accept(listeningSocket, (struct sockaddr*)&clientAddr, &socketLength);
		thread(clientHandler, connectionSocket, clientAddr).detach();
	}
	close(listeningSocket);
	return 0;
}

int	Server::stop()
{
	close(connectionSocket);
	close(listeningSocket);
	serverStatus = false;
	cout << endl << "Server has been stopped" << endl;
	return 0;
}

void	clientHandler(int clientSocket, struct sockaddr_in clientInfo)
{
	char	ip[16];
	int	port;

	inet_ntop(AF_INET, &clientInfo.sin_addr, ip, sizeof(ip));
	port = ntohs(clientInfo.sin_port);
	cout << "<New connection from " << ip << ":" << port << ">" << endl;
	/*
		Do something here
	*/
	close(clientSocket);
	cout << "<Client disconnected (" << ip << ":" << port << ")>" << endl;
}
