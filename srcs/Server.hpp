#ifndef	SERVER_HPP
#define	SERVER_HPP

#include	"Headers.hpp"

using namespace	std;

void	clientHandler(int, struct sockaddr_in);

class	Server
{
	private:

	int			listeningPort;
	int			listeningSocket;
	int			connectionSocket;
	socklen_t		socketLength;
	struct sockaddr_in	serverAddr;
	struct sockaddr_in	clientAddr;
	bool			serverStatus;

	public:

	Server(int);
	~Server();
	bool			isStarted()const;
	void			setListeningPort(int);
	int			getListeningPort()const;
	int			start();
	int			stop();
};

#endif
