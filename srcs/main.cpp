#include	"Server.hpp"

using namespace	std;

Server*	server;

void	sig_handler(int s)
{
	server->stop();
	exit(s);
}

int	main(int ac, char** av)
{
	struct sigaction sigIntHandler;

	sigIntHandler.sa_handler = sig_handler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;

	if (ac != 2)
	{
		cerr << "Usage : " << av[0] << " PORT" << endl;
		return -1;
	}

	server = new Server(atoi(av[1]));

	sigaction(SIGINT, &sigIntHandler, NULL);

	server->start();

	return 0;
}
