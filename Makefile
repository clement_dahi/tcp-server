CXX	= g++
CXXFLAGS= -Wall -std=c++11 -pedantic
LDFLAGS	=
EXEC	= tcpServer
SRC	= $(wildcard ./srcs/*.cpp)
OBJ	= $(SRC:.cpp=.o)

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CXX) $(LDFLAGS) -o ./bin/$@ $^ -lpthread

.PHONY: clean fclean

clean:
	rm -rf ./srcs/*.o

fclean: clean
	rm -rf ./bin/$(EXEC)

